default["apache"]["sites"]["nallamalahk3"] = {"site_title" => "Nallamala 3 Website", "port" => 80, "domain" => "nallamalahk3.mylabserver.com"}
default["apache"]["sites"]["nallamalahk3b"] = {"site_title" => "Hari Nallamala 3b Website","port" => 80, "domain" => "nallamalahk3b.mylabserver.com"}

case node["platform"]
when "centos"
	default["apache"]["package"] = "httpd"
when "ubuntu"
	default["apache"]["package"] = "apache2"
end
